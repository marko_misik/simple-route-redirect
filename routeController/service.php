<?php

class Route extends routeController {

	public $route;
	protected $target;
	protected $statusCode;
	public $map = array();

	public function __construct ($route = null) {
		if($route != null) {
			$this->route = $route;
		}
	}

	public function doAction() {
		$target = parent::getRouteResult();

		if(!empty($target) and is_array($target)) {
			$this->target     = $target['url'];
			$this->statusCode = $target['code'];
			self::redirect();
		}
	}

	public function redirect() {
		header("HTTP/1.1 " . $this->statusCode . " Moved Permanently");
        http_response_code($this->statusCode);
        header('Location: /' . $this->target , true, $this->statusCode);
        exit;
	}


}

?>