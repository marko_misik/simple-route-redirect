<?php

class routeController {

	protected $delimiter = '-';

	protected function getRouteResult() {
		if(array_key_exists($this->normalizeUrl(), $this->map)) {
			return $this->map[$this->normalizeUrl()];
		}
	}

	protected function normalizeUrl() {
		setlocale(LC_ALL, 'en_US.UTF8');

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $this->route);
		$clean = ltrim($clean, '/');

		return $clean;
	}

}

?>