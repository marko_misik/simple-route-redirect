<?php
/*
* PAGE ROUTE MAP
* router match array key and redirect it to the array value
*/

return array(

	"first-route/add-faf" => array(
		"url"  => "route-after-redirect",
		"code" => "301",
	),
	"second-route" => array(
		"url"  => "route-after-redirect",
		"code" => "301",
	),
	"third-route" => array(
		"url"  => "route-after-redirect",
		"code" => "301",
	),

);

?>