<?php

/*
* check for existing controller directory
*/
if(!is_dir(__DIR__ . DIRECTORY_SEPARATOR . './routeController/')) {
	die('Not found controller directory.');
}

/*
* check if route controller exists
*/
if(!file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'routeController/controller.php')) {
	die('Need a controller.');
}
include_once __DIR__ . DIRECTORY_SEPARATOR . 'routeController/controller.php';

/*
* check if route controller exists
*/
if(!file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'routeController/service.php')) {
	die('Need route services.');
}
include_once __DIR__ . DIRECTORY_SEPARATOR . 'routeController/service.php';

$Route = new Route($_SERVER['REQUEST_URI']);

/*
* check if route map exists
*/
if(!file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'routeController/routeMap.php')) {
	die('Need a route map array with redirects.');
}
$Route->map = include_once __DIR__ . DIRECTORY_SEPARATOR . 'routeController/routeMap.php';

$Route->doAction();

?>